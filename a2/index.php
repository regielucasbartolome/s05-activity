<?php session_start(); ?>

<?php if (isset($_SESSION['userLogin'])) {
	$message = $_SESSION['userLogin']['email'];
} else {
	header("location: ./login.php");
}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Landing Page</title>
</head>
<body>
	<h1>Hello, <?php echo $message; ?></h1>
	<button><a href="logout.php">Logout</a></button>
</body>
</html>